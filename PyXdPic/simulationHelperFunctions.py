#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np
from scipy import ndimage
import nibabel as nib
from alignArray import align3DArray
import gzip
import pickle


def materialMapsFromImages( materialImg, 
                            airIntensity, fatIntensity, glandIntensity, chestIntensity, muscleIntensity,
                            fatMu=0.1e3, fatLa=1.0e4, 
                            glandMu=0.2e3, glandLa=1.0e4, 
                            airMu=0.1e2, airLa=1.0e4, 
                            chestMu=1.0e4, chestLa=1.0e5,
                            muscleMu=1.0e4, muscleLa=1.0e5,
                            forceX=0.0, forceY=0.0, forceZ=0.0, 
                            airDampingScale=1.0 ) :
    
    materialMuMap = np.zeros_like( materialImg, dtype=np.float )
    materialLaMap = np.zeros_like( materialImg, dtype=np.float )
    forceXMap     = np.zeros_like( materialImg, dtype=np.float )
    forceYMap     = np.zeros_like( materialImg, dtype=np.float )
    forceZMap     = np.zeros_like( materialImg, dtype=np.float )
    dampScaleMap  = np.ones_like ( materialImg, dtype=np.float )
    
    materialMuMap[ :                              ] = airMu
    materialMuMap[ materialImg == fatIntensity    ] = fatMu
    materialMuMap[ materialImg == glandIntensity  ] = glandMu
    materialMuMap[ materialImg == chestIntensity  ] = chestMu
    materialMuMap[ materialImg == muscleIntensity ] = muscleMu
    
    materialLaMap[ :                              ] = airLa
    materialLaMap[ materialImg == fatIntensity    ] = fatLa
    materialLaMap[ materialImg == glandIntensity  ] = glandLa
    materialLaMap[ materialImg == chestIntensity  ] = chestLa
    materialLaMap[ materialImg == muscleIntensity ] = muscleLa

    forceXMap[ np.bitwise_or( np.bitwise_or( materialImg==glandIntensity, materialImg==fatIntensity), materialImg==muscleIntensity ) ] = forceX
    forceYMap[ np.bitwise_or( np.bitwise_or( materialImg==glandIntensity, materialImg==fatIntensity), materialImg==muscleIntensity ) ] = forceY
    forceZMap[ np.bitwise_or( np.bitwise_or( materialImg==glandIntensity, materialImg==fatIntensity), materialImg==muscleIntensity ) ] = forceZ
    
    dampScaleMap[ materialImg == airIntensity ] = airDampingScale
    
    return materialMuMap, materialLaMap, forceXMap, forceYMap, forceZMap, dampScaleMap




def mask2strip( imageIn, width=0.25 ):
    return np.array( np.invert( np.bitwise_and( imageIn>0.5-width,imageIn<0.5+width)), dtype=np.int )




def mask2smoothStrip( imageIn, width, sigma ):
    smoothImage = ndimage.gaussian_filter(imageIn, sigma )
    return np.array( np.invert( np.bitwise_and( smoothImage>0.5-width,smoothImage<0.5+width)), dtype=np.int )




def saveNiftiImage( fileName, imageData, affine, header=None ):
    ''' Save an array to a nifti file. 
    
        :param fileName: The output file path
        :type fileName: string
        :param imageData: The array with the actual image data.
        :type imageData: np.array
        :param affine: The affine 4x4 matrix which defines the index-to-point transfromation.
        :type affine: np.array
        :param header: The nifti image header (optional, look at nibabel for more details)
    '''
    try:
        img = nib.Nifti1Image( imageData, affine, header )
        nib.save(img, fileName )
    except:
        print( 'ERROR: Could not save nifti file to %s' % fileName )
    
    
def rotateDVF( ux, uy, uz, rotMat ):
    #
    # R u = uP
    #
    ux1D = ux.reshape(-1)
    uy1D = uy.reshape(-1)
    uz1D = uz.reshape(-1)
    
    U =  np.array([ux1D, uy1D, uz1D])
    URot = np.dot( rotMat, U ) 
    
    uxRot = align3DArray( URot[0].reshape( ux.shape ) )
    uyRot = align3DArray( URot[1].reshape( ux.shape ) )
    uzRot = align3DArray( URot[2].reshape( ux.shape ) )

    return uxRot, uyRot, uzRot

    
def calculateScaledAffineTransformation( affineTrafo, numberOfLevels, currentLevel ):
    ''' Calculate the scaling of an affine transfomration. 
    
        :param affineTrafo: Numpy array of size 4x4 which holds the affine transfomration
        :param numberOfLevels: The total number of pyramid levels.
        :param currentLevel: The current pyramid level.
    '''
    scaling = np.array( [ [2**(numberOfLevels-currentLevel-1), 0.0, 0.0],
                          [0, 2**(numberOfLevels-currentLevel-1), 0.0],
                          [0.0,0.0,2**(numberOfLevels-currentLevel-1)]])
    
    scaledAffineTrafo        = affineTrafo.copy()
    scaledAffineTrafo[:3,:3] = np.dot( affineTrafo[:3, :3], scaling )
    
    return scaledAffineTrafo


     
def unpickleFDM( filename ):
    '''
        @summary: Read an fdm simulation object from a gzipped, pickled file. The cpp extension is also rebuild to 
                  revalidate references to dll.  
    '''
    with gzip.open(filename, 'rb' ) as f:
        fdm = pickle.load(f)
        f.close()
        
    fdm.reInitCppExtension()
    return fdm

