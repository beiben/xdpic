#! /usr/bin/env python 
# -*- coding: utf-8 -*-

import numpy as np



class niftiTransformationGenerator:
    ''' Central location to create the transformation matrices from image to world and world to image correspondences.
    '''
    
    def __init__( self, nibabelImageIn, nibAffineMatrix=None ):
        ''' Create the transformation 
            
            :param nibabelImageIn: The nibabel image which has to have the function get_affine()
            :type nibabelImageIn: nibabel image
            :param nibAffineMatrix: Affine matrix. If this is specified the image is ignored!
            :type nibAffineMatrix: np.matrix 
            :note: If the affine matrix is given, the specified nibabel image is NOT used, but the matrix directly defines the 
                   transformation. Use with care.  
        '''
        self.rot90z = np.identity( 4, np.float64 )
        self.rot90z[0,0] = -1
        self.rot90z[1,1] = -1
        
        
        if nibAffineMatrix is None:
            self.indexToCoordinateMatrix = np.dot( self.rot90z, nibabelImageIn.affine ) # aka. image 2 world
            self.coordinateToIndexMatrix = np.linalg.inv( self.indexToCoordinateMatrix )      # aka. world 2 image 
        else:
            self.indexToCoordinateMatrix = np.dot( self.rot90z, nibAffineMatrix ) # aka. image 2 world
            self.coordinateToIndexMatrix = np.linalg.inv( self.indexToCoordinateMatrix )      # aka. world 2 image 
        
        

    
    def coordinate2Index( self, x, y, z ) :
        
        cds = np.array( (x, y, z, 1) )
        idx = np.array( np.dot( self.coordinateToIndexMatrix, cds ) + 0.5, dtype = np.int )[:-1]
        
        return idx[0], idx[1], idx[2]



        
    def coordinate2ContinuousIndex( self, x, y, z ) :
        
        # continuous index
        cds = np.array( (x, y, z, 1) )
        cIdx = np.array( np.dot( self.coordinateToIndexMatrix, cds ), dtype = np.float )[:-1]
        
        return cIdx[0], cIdx[1], cIdx[2]
        


        
    def index2Coordinate( self, i, j, k ) :
        
        idx = np.array( (i, j, k, 1) )
        cds = np.dot( self.indexToCoordinateMatrix, idx )
        
        return cds[0], cds[1], cds[2]
    
        
        
        
    def indexArrays2CoordinateArrays( self, iArray, jArray, kArray ):
        ''' Provide three arrays which hold the indices and return the corresponding coordinate arrays.
        
            :param iArray: 1D array with assumed shape (N,) and index values which are to be transformed to coordinates. 
            :param jArray: see iArray
            :param kArray: see iArray
            :return: Array of size 4xN which holds the transformed coordinates. Split if required.
        '''
        
        idxs = np.vstack( ( iArray, jArray, kArray, np.ones( iArray.shape[0] ) ) )
        cdsArray = np.dot( self.indexToCoordinateMatrix, idxs )
        return cdsArray 
        
        
        
        
    def pointArray2ContinuousIndexArray( self, pointArray ):
        ''' Expects an Nx3 array, which will be transformed to an index array of the same size
        
            :param pointArray: The numpy array of size Nx3 wich holds the N points to transform into index coordinates
            :type pointArray: numpy.array
            :return: Array of size Nx3 with the continuous indices
        '''
        
        # Convert the point array to homogeneous coordinates and then transform to index array
        pointArrayHom = np.hstack( ( pointArray,  np.ones( (pointArray.shape[0],1) ) ) )
        continuousIndexArray = np.dot( self.coordinateToIndexMatrix, pointArrayHom.transpose() )
        
        return continuousIndexArray[:3,:].transpose()   
    
    
    