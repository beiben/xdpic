#!/usr/bin/python
# -*- coding: utf-8 -*-


import numpy as np


def _align3DArray( npArrayIn ):
    x,y,z = npArrayIn.shape
    dtype = np.dtype(np.float64)
    nbytes = x * y * z* dtype.itemsize
    buf = np.empty(nbytes + 16, dtype=np.uint8)
    start_index = -buf.ctypes.data % 16
    a = buf[start_index:start_index + nbytes].view(dtype).reshape(x, y, z)
    a[:,:,:] = npArrayIn[:,:,:] # copy the contents into a 
    return a

def align3DArray( npArrayIn ):
    return np.require( npArrayIn, requirements=['C', 'W', 'A', 'O'] )

