import ctypes
from ctypes import cdll

import numpy as np
# import plotSubplot as pltSuplt
try:
    lib = cdll.LoadLibrary( 'fdmHyperElasticExtension.dll' )
    lib.fdmCPPExtension_doTimeStepNH.restype = ctypes.c_bool
    lib.fdmCPPExtension_returnValue.restype  = ctypes.c_double
except:
    print("Warning: Could not load dll library. Using FDM cpp-extension without cpp module.")
    

class fdmCPPExtension( object ):
    
    def __init__(self, dx, dy, dz, mx, my, mz):
        '''
            @summary: Call the constructor of the extension library
        '''
        self.obj = lib.fdmCPPExtension_new( ctypes.c_double(dx), ctypes.c_double(dy), ctypes.c_double(dz),
                                            ctypes.c_int(mx), ctypes.c_int(my),ctypes.c_int(mz) )

    
    def printInfo(self):
        '''
            @summary: Print some basic information...
        '''
        lib.fdmCPPExtension_printInfo( self.obj )


    def diff3D_x(self, npArrayIn, npArrayOut):
        lib.fdmCPPExtension_diff3D_x( self.obj, npArrayIn.ctypes, npArrayOut.ctypes )

    def diff3D_y(self, npArrayIn, npArrayOut):
        lib.fdmCPPExtension_diff3D_y( self.obj, npArrayIn.ctypes, npArrayOut.ctypes )

    def diff3D_z(self, npArrayIn, npArrayOut):
        lib.fdmCPPExtension_diff3D_z( self.obj, npArrayIn.ctypes, npArrayOut.ctypes )

    def diff3D_xx(self, npArrayIn, npArrayOut):
        lib.fdmCPPExtension_diff3D_xx( self.obj, npArrayIn.ctypes, npArrayOut.ctypes )
    
    def diff3D_yy(self, npArrayIn, npArrayOut):
        lib.fdmCPPExtension_diff3D_yy( self.obj, npArrayIn.ctypes, npArrayOut.ctypes )
        
    def diff3D_zz(self, npArrayIn, npArrayOut):
        lib.fdmCPPExtension_diff3D_zz( self.obj, npArrayIn.ctypes, npArrayOut.ctypes )
        
    def diff3D_xy(self, npArrayIn, npArrayOut):
        lib.fdmCPPExtension_diff3D_xy( self.obj, npArrayIn.ctypes, npArrayOut.ctypes )
        
    def diff3D_xz(self, npArrayIn, npArrayOut):
        lib.fdmCPPExtension_diff3D_xz( self.obj, npArrayIn.ctypes, npArrayOut.ctypes )

    def diff3D_yz(self, npArrayIn, npArrayOut):
        lib.fdmCPPExtension_diff3D_yz( self.obj, npArrayIn.ctypes, npArrayOut.ctypes )
        
    def doTimeStepNH(self, ux_prev, uy_prev, uz_prev, 
                           ux_cur,  uy_cur,  uz_cur,
                           ux_next, uy_next, uz_next, 
                           mu, dxMu, dyMu, dzMu,
                           la, dxLa, dyLa, dzLa, 
                           forceX, forceY, forceZ, 
                           forceXIncr, forceYIncr, forceZIncr, 
                           weight1, weight2, weight3, 
                           rho, loadingValue, loadingValueForceIncrement ):
        folding = lib.fdmCPPExtension_doTimeStepNH( self.obj, ux_prev.ctypes, uy_prev.ctypes, uz_prev.ctypes, 
                                                    ux_cur.ctypes,  uy_cur.ctypes,  uz_cur.ctypes,
                                                    ux_next.ctypes, uy_next.ctypes, uz_next.ctypes, 
                                                    mu.ctypes, dxMu.ctypes, dyMu.ctypes, dzMu.ctypes,
                                                    la.ctypes, dxLa.ctypes, dyLa.ctypes, dzLa.ctypes, 
                                                    forceX.ctypes, forceY.ctypes, forceZ.ctypes, 
                                                    forceXIncr.ctypes, forceYIncr.ctypes, forceZIncr.ctypes, 
                                                    weight1.ctypes, weight2.ctypes, weight3.ctypes, 
                                                    rho.ctypes, ctypes.c_double( loadingValue ), 
                                                    ctypes.c_double( loadingValueForceIncrement ) )
        return folding
        
        
    def warpImageOfDifferentResolutionWithDVF( self, ux, uy, uz, 
                                                     imageIn, warpedImgOut, 
                                                     warpImageToWorldMatrix, warpWorldToImageMatrix,
                                                     dvfWorldToImageMatrix,
                                                     imgSizeX, imgSizeY, imgSizeZ):
        lib.fdmCPPExtension_warpImageOfDifferentResolutionWithDVF( self.obj, ux.ctypes, uy.ctypes, uz.ctypes,
                                                                   imageIn.ctypes, warpedImgOut.ctypes, 
                                                                   warpImageToWorldMatrix.ctypes, warpWorldToImageMatrix.ctypes,
                                                                   dvfWorldToImageMatrix.ctypes,
                                                                   ctypes.c_int( imgSizeX ), ctypes.c_int( imgSizeY ), ctypes.c_int( imgSizeZ )  )



    def returnValue(self, npArrayIn, iX, iY, iZ):
        return lib.fdmCPPExtension_returnValue( self.obj, npArrayIn.ctypes, ctypes.c_int(iX), ctypes.c_int(iY), ctypes.c_int(iZ) )
        
    
    
if __name__ == '__main__' :
    pass
    
#     def checkDerivative( A, direction='x', dx=1.0, dy=1.1, dz=1.2, repeatNTimes=10 ):
#     
#         # Generate the output arrays
#         B_py  = np.zeros_like( A )
#         B_cpp = np.zeros_like( A )
#     
#         # python differentiation
#         pyDiff = fd.finiteDifferences3D( dx, dy, dz )
#         
#         if direction == 'x':
#             tic = time.time() 
#             for _ in xrange( repeatNTimes ):
#                 pyDiff.diffXC(A, B_py)
#             toc = time.time()
#             
#         elif direction == 'y':
#             tic = time.time() 
#             for _ in xrange( repeatNTimes ):
#                 pyDiff.diffYC(A, B_py)
#             toc = time.time()
#         
#         elif direction == 'z':
#             tic = time.time() 
#             for _ in xrange( repeatNTimes ):
#                 pyDiff.diffZC(A, B_py)
#             toc = time.time()
# 
#         elif direction == 'xx':
#             tic = time.time() 
#             for _ in xrange( repeatNTimes ):
#                 pyDiff.diffXX(A, B_py)
#             toc = time.time()
# 
#         elif direction == 'yy':
#             tic = time.time() 
#             for _ in xrange( repeatNTimes ):
#                 pyDiff.diffYY(A, B_py)
#             toc = time.time()
#         
#         elif direction == 'zz':
#             tic = time.time() 
#             for _ in xrange( repeatNTimes ):
#                 pyDiff.diffZZ(A, B_py)
#             toc = time.time()
#         
#         elif direction == 'xy':
#             tic = time.time() 
#             for _ in xrange( repeatNTimes ):
#                 pyDiff.diffXY(A, B_py)
#             toc = time.time()
#         
#         elif direction == 'xz':
#             tic = time.time() 
#             for _ in xrange( repeatNTimes ):
#                 pyDiff.diffXZ(A, B_py)
#             toc = time.time()
#         
#         elif direction == 'yz':
#             tic = time.time() 
#             for _ in xrange( repeatNTimes ):
#                 pyDiff.diffYZ(A, B_py)
#             toc = time.time()
#         pyTime = toc-tic
# 
#         # c++ differentiation
#         f = fdmCPPExtension(dx, dy, dz, mx, my, mz)
#         f.printInfo()
#         
#         
#         if direction == 'x' :
#             tic = time.time() 
#             for _ in xrange(repeatNTimes):
#                 f.diff3D_x(A, B_cpp)
#             toc = time.time()
# 
#         if direction == 'y' :
#             tic = time.time() 
#             for _ in xrange(repeatNTimes):
#                 f.diff3D_y(A, B_cpp)
#             toc = time.time()
# 
#         if direction == 'z' :
#             tic = time.time() 
#             for _ in xrange(repeatNTimes):
#                 f.diff3D_z(A, B_cpp)
#             toc = time.time()
# 
#         if direction == 'xx' :
#             tic = time.time() 
#             for _ in xrange(repeatNTimes):
#                 f.diff3D_xx(A, B_cpp)
#             toc = time.time()
# 
#         if direction == 'yy' :
#             tic = time.time() 
#             for _ in xrange(repeatNTimes):
#                 f.diff3D_yy(A, B_cpp)
#             toc = time.time()
# 
#         if direction == 'zz' :
#             tic = time.time() 
#             for _ in xrange(repeatNTimes):
#                 f.diff3D_zz(A, B_cpp)
#             toc = time.time()
# 
#         if direction == 'xy' :
#             tic = time.time() 
#             for _ in xrange(repeatNTimes):
#                 f.diff3D_xy(A, B_cpp)
#             toc = time.time()
# 
#         if direction == 'xz' :
#             tic = time.time() 
#             for _ in xrange(repeatNTimes):
#                 f.diff3D_xz(A, B_cpp)
#             toc = time.time()
# 
#         if direction == 'yz' :
#             tic = time.time() 
#             for _ in xrange(repeatNTimes):
#                 f.diff3D_yz(A, B_cpp)
#             toc = time.time()
#         
#         cppTime = toc-tic
#     
#         # Check the difference betweeen the evaluations...
#         errorPyCpp  = B_py  - B_cpp 
#         maxE = np.max( errorPyCpp )
#         minE = np.min( errorPyCpp )
#         
#         return pyTime, cppTime, minE, maxE, B_py, B_cpp
# 
# 
# 
#     
#     import matplotlib.pyplot as plt
#     
#     mx = 256 
#     my = 220 
#     mz = 124
#     
#     dx = 1.2
#     dy = 1.102
#     dz = 1.5
#     
#     repeatNTimes = 1
#     
#     
#     x = np.mgrid[0:mx,0:my, 0:mz]
#     #A = np.sin( x[0] * 2.0 * np.pi / mx ) * np.sin( x[1] * 2.0 * np.pi / my ) * np.sin( x[2] * 2.0 * np.pi / mz )
#     A = np.random.rand( mx, my, mz )
#     
#     #
#     # Check if the simple access or the array is correct...
#     #
#     c = fdmCPPExtension(dx, dy, dz, mx, my, mz)
#     c.returnValue(A, 10, 11, 12)
#     
#     
#     
#     directions = ['x','y','z', 'xx', 'yy', 'zz', 'xy', 'xz', 'yz']
#     pyTimes    = []
#     cppTimes   = []
#     minErrors  = []
#     maxErrors  = []
#     
#     for d in directions:
#     
#         pyTime, cppTime, minE, maxE, Bp, Bc = checkDerivative(A, d, dx, dy, dz, repeatNTimes)
#         
#         pyTimes.append(pyTime)
#         cppTimes.append(cppTime)
#         minErrors.append(minE)
#         maxErrors.append(maxE)
# 
#     # print the summary
#     print( ' Results ' )
#     print( '=========' )
#     print( ' direction   | time python | time cpp    | min error   | max error   | speedup   ' )
#     print( '-------------+-------------+-------------+-------------+-------------+-----------' )
#     
#     for i in range(len(directions)):
#         print( ' %3s         | %.4f      | %.4f      | %+.4e | %+.4e | %.4f ' %(directions[i], pyTimes[i], cppTimes[i], minErrors[i], maxErrors[i], pyTimes[i]/cppTimes[i] ) )
#         
#     
    
