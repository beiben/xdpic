#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
@author:  Bjoern Eiben
@summary: Implements a class which allows to detect and correct folding of displacement vector fields
'''

import finiteDifferences as finiteDif
import numpy as np
from scipy import ndimage
import niftiTransformationGenerator as ntg
import fdmCppExtension as fdmCPP
from alignArray import align3DArray
from simulationHelperFunctions import saveNiftiImage


class DisplacementVectorFieldTools3D( finiteDif.finiteDifferences3D ):
    '''
    @summary: Class to perform basic operations on 3D displacement vector fields (DVFs) given on a regular grid.
              These include DVF inversion, folding detection and correction. 
    '''
    
    def __init__( self, dx, dy, dz ):
        '''
            @param dx: Grid spacing of the displacement vector field in x-direction.
            @param dy: Grid spacing of the displacement vector field in y-direction.
            @param dz: Grid spacing of the displacement vector field in z-direction.
            @note: Change the parameter self.minAllowedJacobian to increase folding sensitivity.
        '''
        finiteDif.finiteDifferences3D.__init__( self, dx, dy, dz )
        self.minAllowedJacobian = 0.05
        self.smoothingRadius = 1.0
        self.verbose = False
        
        
        
        
    def checkForFolding( self, ux, uy, uz ):
        '''
            @summary: Analyses the given DVF and checks if folding occurred.
            @param ux: x-component of the DVF
            @param uy: y-component of the DVF
            @param uz: z-component of the DVF
        '''
        # Test for folding by using the deformation gradient and its determinant
        F11 = np.zeros_like(ux)
        F21 = np.zeros_like(ux)
        F31 = np.zeros_like(ux)

        F12 = np.zeros_like(ux)
        F22 = np.zeros_like(ux)
        F32 = np.zeros_like(ux)

        F13 = np.zeros_like(ux)
        F23 = np.zeros_like(ux)
        F33 = np.zeros_like(ux)

        self.diffX( ux, F11 )
        self.diffX( uy, F21 )
        self.diffX( uz, F31 )

        self.diffY( ux, F12 )
        self.diffY( uy, F22 )
        self.diffY( uz, F32 )

        self.diffZ( ux, F13 )
        self.diffZ( uy, F23 )
        self.diffZ( uz, F33 )
        
        # Add 1.0 to main diagonal as: F = I + grad(u)
        F11 = 1.0 + F11
        F22 = 1.0 + F22
        F33 = 1.0 + F33
        
        # Derive the Jacobian from this
        subDet1 = (F22 * F33 - F23 * F32)
        subDet2 = (F21 * F33 - F23 * F31)
        subDet3 = (F21 * F32 - F22 * F31)
        
        J = (   F11 * subDet1 
              - F12 * subDet2
              + F13 * subDet3 ) 
        
        foldingElements = np.where( J<=self.minAllowedJacobian )
        folding = foldingElements[0].shape[0] > 0 
        
        if folding and self.verbose:
            print('Detected folding: min(J) = %e, numElements invovled: %i' %(J.min(), foldingElements[0].shape[0]) )
            
        return folding, foldingElements




    def correctFolding( self, ux, uy, uz, enforceZeroDisplacementBoundary=True ):
        
        self.correct3dNAN( ux, 2 )
        self.correct3dNAN( uy, 2 )
        self.correct3dNAN( uz, 2 )
        
        [folding, foldingElements] = self.checkForFolding( ux, uy, uz )
        #TODO: Find where ux/uy/uz are NaN and fill with closest displacement...
        numFoldingCorrectionSteps = 0
        
        while folding:
            numFoldingCorrectionSteps += 1
            #
            # Correct folding
            # Idea: Locally smooth the DVF until no more folding occurs    
            #
            foldingMask = np.zeros_like( ux )
            foldingMask[foldingElements] = 1.0 
            
            smoothFoldingMask = ndimage.gaussian_filter( foldingMask, self.smoothingRadius )
            
            uxUpSmooth = ndimage.gaussian_filter( ux, 1.0 )
            uyUpSmooth = ndimage.gaussian_filter( uy, 1.0 )
            uzUpSmooth = ndimage.gaussian_filter( uz, 1.0 )
            
            if enforceZeroDisplacementBoundary:
                self._setBorderToZeroDisplacement( uxUpSmooth )
                self._setBorderToZeroDisplacement( uyUpSmooth )
                self._setBorderToZeroDisplacement( uzUpSmooth )
            
            uxNew = ux * (1.0 - smoothFoldingMask) + uxUpSmooth * (smoothFoldingMask)
            uyNew = uy * (1.0 - smoothFoldingMask) + uyUpSmooth * (smoothFoldingMask)
            uzNew = uz * (1.0 - smoothFoldingMask) + uzUpSmooth * (smoothFoldingMask)
            
            # check again for folding
            folding, foldingElements = self.checkForFolding( uxNew, uyNew, uzNew )
            
            ux = uxNew
            uy = uyNew
            uz = uzNew
        
        if self.verbose:
            print('Folding DVF was corrected in %i iterations.' % numFoldingCorrectionSteps )
        
        return ux, uy, uz



    def _setBorderToZeroDisplacement( self, arrayIn ):
        arrayIn[:,:, 0] = 0.0
        arrayIn[:,:,-1] = 0.0
        arrayIn[:, 0,:] = 0.0
        arrayIn[:,-1,:] = 0.0
        arrayIn[ 0,:,:] = 0.0
        arrayIn[-1,:,:] = 0.0
        
        
        
        
    def correct3dNAN( self, arrayIn, minSummation = 2 ):
        '''
            @summary: Remove NaN elements from an array by interpolating by the closest neighbours. 
            @param arrayIn: The array which will be corrected
            @param minSummation: The minimum number of neighbours that have to be different from NaN before a summation is being performed.
                                 Valid options are 1, 2 and 3. If set higher, the algorithm will only converge in very limited scenarios.  
        '''
        
        idxNaN = np.where( np.isnan( arrayIn ) )
        
        max_x = arrayIn.shape[0]-1
        max_y = arrayIn.shape[1]-1
        max_z = arrayIn.shape[2]-1
        
        aCorrected = arrayIn.copy()
        
        while np.max(np.isnan(arrayIn)):
            idxNaN = np.where(np.isnan(arrayIn))
            
            for i in range( idxNaN[0].shape[0] ):
                
                curIDX_x = idxNaN[0][i]
                curIDX_y = idxNaN[1][i]
                curIDX_z = idxNaN[2][i]
                
                curIDX_x_low = np.clip( curIDX_x-1, 0, max_x )
                curIDX_y_low = np.clip( curIDX_y-1, 0, max_y )
                curIDX_z_low = np.clip( curIDX_z-1, 0, max_z )
        
                curIDX_x_high = np.clip( curIDX_x+1, 0, max_x )
                curIDX_y_high = np.clip( curIDX_y+1, 0, max_y )
                curIDX_z_high = np.clip( curIDX_z+1, 0, max_z )
                
                # compose indices of six nearest neighbours
                idxNN1 = ( curIDX_x_low,  curIDX_y,      curIDX_z      )
                idxNN2 = ( curIDX_x_high, curIDX_y,      curIDX_z      )
                idxNN3 = ( curIDX_x,      curIDX_y_low,  curIDX_z      ) 
                idxNN4 = ( curIDX_x,      curIDX_y_high, curIDX_z      )
                idxNN5 = ( curIDX_x,      curIDX_y,      curIDX_z_low  )
                idxNN6 = ( curIDX_x,      curIDX_y,      curIDX_z_high )
        
                idxNNs = [ idxNN1, idxNN2, idxNN3, idxNN4, idxNN5, idxNN6 ]
        
                numSums = 0
                valSum  = 0.0
                
                for idxNN in idxNNs:
                    valNN = arrayIn[idxNN]
                
                    if not np.isnan( valNN ):
                        numSums += 1
                        valSum += valNN
                
                if numSums >= minSummation:
                    aCorrected[ curIDX_x, curIDX_y, curIDX_z ] = valSum / (1.0 * numSums)
                    
            arrayIn[:,:] = aCorrected[:,:]
            
        del aCorrected

        
    def invertDVF3D( self, ux, uy, uz, tolerance, maxIterations=20 ):
        '''
            @summary: Invert a three dimensional displacement vector field. This implementation is based on 
                      the matlab version 
            @param ux: Displacement in x direction
            @param uy: Displacement in y direction
            @param uz: Displacement in z direction
            @param spacing: numpy array with three elements holding the voxel spacing in the corresponding directions
            @param tolerance: Tolerance of the update, controls the convergence
        '''
        
        # The indices
        xT, yT, zT = np.mgrid[ 0 : ux.shape[0], 
                               0 : ux.shape[1], 
                               0 : ux.shape[2] ] 
    
        xT = xT.astype( np.float )
        yT = yT.astype( np.float )
        zT = zT.astype( np.float )
        
        # transform the displacement into index coordinates
        uX = ux / self.dx
        uY = uy / self.dy
        uZ = uz / self.dz
        
        xROld = xT.copy()
        yROld = yT.copy()
        zROld = zT.copy()
        
        uxP = self._interp3( uX, xROld, yROld, zROld ) 
        uyP = self._interp3( uY, xROld, yROld, zROld ) 
        uzP = self._interp3( uZ, xROld, yROld, zROld ) 
        
        # Make sure the outside values (indicated by np.nan) are set to zero 
        uxP[ np.isnan(uxP) ] = 0.0
        uyP[ np.isnan(uyP) ] = 0.0
        uzP[ np.isnan(uzP) ] = 0.0
    
        # Update the DVF first guess
        xR = xT - uxP
        yR = yT - uyP
        zR = zT - uzP
        
        step = 1
    
        idxSet = np.invert( np.isnan( ux ) ) 
        
        # This deviates from the MATLAB implementation!
        #
        # idxSet here is a bool array so that intersection can be done by simple ... bool operations
        # 
        
        # evaluate the update magnitude
        normX = self.dx * np.linalg.norm( xR[idxSet]-xROld[ idxSet ], np.inf )
        normY = self.dy * np.linalg.norm( yR[idxSet]-yROld[ idxSet ], np.inf )
        normZ = self.dz * np.linalg.norm( zR[idxSet]-zROld[ idxSet ], np.inf )
    
        # iterate to converge to a solution
        while ( (normX > tolerance) or (normY > tolerance) or (normZ>tolerance) ) and (step < maxIterations):
            
            if self.verbose:
                print( '%d  %f %f %f\n' % (np.sum(idxSet), normX, normY, normZ) )
            
            xROld[:,:,:] = xR[:,:,:]
            yROld[:,:,:] = yR[:,:,:]
            zROld[:,:,:] = zR[:,:,:]
            
            # interpolate the DVF at the new position
            uxP = self._interp3( uX, xROld, yROld, zROld )
            uyP = self._interp3( uY, xROld, yROld, zROld )
            uzP = self._interp3( uZ, xROld, yROld, zROld )
            
            
            # exclude out of bounds values from further evaluation
            idxSet = np.bitwise_and(np.invert(np.isnan( uxP )), idxSet)
        
            # set out of bounds value to zero
            uxP[np.isnan(uxP)] = 0.0
            uyP[np.isnan(uyP)] = 0.0
            uzP[np.isnan(uzP)] = 0.0
            
            # update the inverse estimate
            
            xR = xT - uxP
            yR = yT - uyP
            zR = zT - uzP
            
            step += 1
            
            # evaluate the update magnitude
            normX = self.dx * np.linalg.norm( xR[idxSet]-xROld[ idxSet ], np.inf )
            normY = self.dy * np.linalg.norm( yR[idxSet]-yROld[ idxSet ], np.inf )
            normZ = self.dz * np.linalg.norm( zR[idxSet]-zROld[ idxSet ], np.inf )
        
        if self.verbose:
            print( '%d  %f %f %f\n' % (np.sum(idxSet), normX, normY, normZ) )
        
        # subtract the identity and convert back into real world distances
        xR[:,:,:] = (xR - xT) * self.dx
        yR[:,:,:] = (yR - yT) * self.dy
        zR[:,:,:] = (zR - zT) * self.dz
        
        return xR, yR, zR
    
    
    

    def _interp3(self, u, x, y, z ):
        ''' 
            @summary: Equivalent to MATLAB function which interpolates an array  
            @param u: 3D array which should be interpolated at the given positions (x,y,z)
            @param x: 3D array holding the x positions at which the input array u should be interpolated.
            @param y: 3D array holding the y positions at which the input array u should be interpolated.
            @param z: 3D array holding the z positions at which the input array u should be interpolated.
        '''
        return ndimage.map_coordinates( u, [x,y,z], order=1, cval=np.nan, mode='constant' ) 


    def warpImageOfDifferentResolutionWithDVF( self, niiAffineMatrixFDM, niiAffineMatrixImage, 
                                               niiImageData, outNiiImageFileName, 
                                               ux, uy, uz ):
        ''' Warp an image with a given DVF. In a first step the current DVF scaled to the 
            size of the specified input image, which in a second step is then warped. 
            
            :param niiAffineMatrixFDM: The transformation matrix for the FDM solver. 
            :type niiAffineMatrixFDM: np.array 
            :param niiAffineMatrixImage: The transformation matrix of the image. 
            :type niiAffineMatrixImage: np.array 
            :param niiImageNameToWarp: Input image which will be warped. 
            :type niiImageNameToWarp: np.array
            :param outNiiImageFileName: If an output file name is specified, then the output image is saved to this location. 
            :type outNiiImageFileName: string
            :param ux: The x-displacement vectorfield. Will be defaulted to current displacement if not given.
            :type ux: np.array
            :param uy: The y-displacement vectorfield. Will be defaulted to current displacement if not given.
            :type uy: np.array
            :param uz: The z-displacement vectorfield. Will be defaulted to current displacement if not given.
            :type uz: np.array
            :note: Make sure that the corresponding nifti transformation was set correctly with setCorrespondingNiftiImageTransformation. 
        '''
        
        self.useCPP = True
        
        if self.useCPP:
            
            self.cppExt = fdmCPP.fdmCPPExtension( 1.0, 1.0, 1.0, ux.shape[0], ux.shape[1], ux.shape[2] )
            self.cppExt.printInfo()
            ntgFDM = ntg.niftiTransformationGenerator( None, niiAffineMatrixFDM  )
            ntgImg = ntg.niftiTransformationGenerator( None, niiAffineMatrixImage)
            
            # Generate the output data
            warpedImgOut = align3DArray( np.zeros_like( niiImageData ) )
            
            # Correct for axis align
#             S = np.array([[self.dx, 0,0],[0,self.dy,0],[0,0,self.dz]]) * 1000.0
#             CorMat = np.linalg.inv( np.dot(S, ntgFDM.coordinateToIndexMatrix[:3,:3]) )
#             
#             uxRot, uyRot, uzRot = rotateDVF(ux, uy, uz, CorMat)
#             
            self.cppExt.warpImageOfDifferentResolutionWithDVF( align3DArray(ux), align3DArray(uy), align3DArray(uz), 
                                                               align3DArray( niiImageData ), warpedImgOut, 
                                                               align3DArray( ntgImg.indexToCoordinateMatrix ), align3DArray( ntgImg.coordinateToIndexMatrix ), 
                                                               align3DArray( ntgFDM.coordinateToIndexMatrix ), 
                                                               niiImageData.shape[0], niiImageData.shape[1], niiImageData.shape[2] )
        

        else:        
            #
            # Generate the transformations for DVF and input image
            #    
            niiDVFTransformation = ntg.niftiTransformationGenerator( None, niiAffineMatrixFDM   )
            ntgFullRes           = ntg.niftiTransformationGenerator( None, niiAffineMatrixImage )
            
            # Get the index limits of the full resolution image
            fullResMaxIDX = niiImageData.shape
            dvfMaxIDX     = ux.shape
            
            idxFullResX, idxFullResY, idxFullResZ = np.mgrid[ :fullResMaxIDX[0], :fullResMaxIDX[1], :fullResMaxIDX[2] ]
            
            idxFullResX = idxFullResX.reshape(-1)
            idxFullResY = idxFullResY.reshape(-1)
            idxFullResZ = idxFullResZ.reshape(-1)
            
            pointsPhysicalSpace = ntgFullRes.indexArrays2CoordinateArrays(idxFullResX, idxFullResY, idxFullResZ)
            idxsDVF   = niiDVFTransformation.pointArray2ContinuousIndexArray( pointsPhysicalSpace.transpose()[:,:3] )
            
            # interpolate the DVF at this location
            continuousIdxs = ( idxsDVF[:,0], idxsDVF[:,1], idxsDVF[:,2] )
            
            # look up the correction displacement and do a linear interpolation
            #  +-->x 
            #  |  v1-----------v4 -  -- 
            #  v  | w3 |   w2  |  ^  Dy
            #  y  |----+-------|     --
            #     |    |       |  1
            #     | w4 |   w1  |      1-Dy
            #     |    |       |  v
            #    v2------------v3 -
            #     |<-    1   ->|
            #     | Dx | 1-Dx  |
            # 
            # w1 = (1-Dx) (1-Dy) (1-Dz)  floor floor floor    000 
            # w2 = (1-Dx) Dy     (1-Dz)  floor ceil  floor    010
            # w3 = Dx     Dy     (1-Dz)  ceil  ceil  floor    110
            # w4 = Dx     (1-Dy) (1-Dz)  ceil floor  floor    100
            # 
            # w5 = (1-Dx) (1-Dy) Dz      floor floor ceil     001
            # w6 = (1-Dx) Dy     Dz      floor ceil  ceil     011
            # w7 = Dx     Dy     Dz      ceil  ceil  ceil     111
            # w8 = Dx     (1-Dy) Dz      ceil  floor ceil     101
            # 
             
            idx1 = [ np.array( np.floor( continuousIdxs[0] ), dtype=np.int ), 
                     np.array( np.floor( continuousIdxs[1] ), dtype=np.int ),
                     np.array( np.floor( continuousIdxs[2] ), dtype=np.int ) ] 
            idx2 = [ np.array( np.floor( continuousIdxs[0] ), dtype=np.int ), 
                     np.array( np.ceil ( continuousIdxs[1] ), dtype=np.int ),
                     np.array( np.floor( continuousIdxs[2] ), dtype=np.int ) ]
            idx3 = [ np.array( np.ceil ( continuousIdxs[0] ), dtype=np.int ), 
                     np.array( np.ceil ( continuousIdxs[1] ), dtype=np.int ),
                     np.array( np.floor( continuousIdxs[2] ), dtype=np.int ) ] 
            idx4 = [ np.array( np.ceil ( continuousIdxs[0] ), dtype=np.int ), 
                     np.array( np.floor( continuousIdxs[1] ), dtype=np.int ),
                     np.array( np.floor( continuousIdxs[2] ), dtype=np.int ) ] 
            idx5 = [ np.array( np.floor( continuousIdxs[0] ), dtype=np.int ), 
                     np.array( np.floor( continuousIdxs[1] ), dtype=np.int ),
                     np.array( np.ceil ( continuousIdxs[2] ), dtype=np.int ) ] 
            idx6 = [ np.array( np.floor( continuousIdxs[0] ), dtype=np.int ), 
                     np.array( np.ceil ( continuousIdxs[1] ), dtype=np.int ),
                     np.array( np.ceil ( continuousIdxs[2] ), dtype=np.int )  ] 
            idx7 = [ np.array( np.ceil ( continuousIdxs[0] ), dtype=np.int ), 
                     np.array( np.ceil ( continuousIdxs[1] ), dtype=np.int ),
                     np.array( np.ceil ( continuousIdxs[2] ), dtype=np.int )  ] 
            idx8 = [ np.array( np.ceil ( continuousIdxs[0] ), dtype=np.int ), 
                     np.array( np.floor( continuousIdxs[1] ), dtype=np.int ),
                     np.array( np.ceil ( continuousIdxs[2] ), dtype=np.int )  ] 
             
            # make sure that the boundaries are not exceeded
            idx1[0] = np.clip( idx1[0], 0, dvfMaxIDX[0]-1 )
            idx2[0] = np.clip( idx2[0], 0, dvfMaxIDX[0]-1 )
            idx3[0] = np.clip( idx3[0], 0, dvfMaxIDX[0]-1 )
            idx4[0] = np.clip( idx4[0], 0, dvfMaxIDX[0]-1 )
            idx5[0] = np.clip( idx5[0], 0, dvfMaxIDX[0]-1 )
            idx6[0] = np.clip( idx6[0], 0, dvfMaxIDX[0]-1 )
            idx7[0] = np.clip( idx7[0], 0, dvfMaxIDX[0]-1 )
            idx8[0] = np.clip( idx8[0], 0, dvfMaxIDX[0]-1 )
             
            idx1[1] = np.clip( idx1[1], 0, dvfMaxIDX[1]-1 )
            idx2[1] = np.clip( idx2[1], 0, dvfMaxIDX[1]-1 )
            idx3[1] = np.clip( idx3[1], 0, dvfMaxIDX[1]-1 )
            idx4[1] = np.clip( idx4[1], 0, dvfMaxIDX[1]-1 )
            idx5[1] = np.clip( idx5[1], 0, dvfMaxIDX[1]-1 )
            idx6[1] = np.clip( idx6[1], 0, dvfMaxIDX[1]-1 )
            idx7[1] = np.clip( idx7[1], 0, dvfMaxIDX[1]-1 )
            idx8[1] = np.clip( idx8[1], 0, dvfMaxIDX[1]-1 )
             
            idx1[2] = np.clip( idx1[2], 0, dvfMaxIDX[2]-1 )
            idx2[2] = np.clip( idx2[2], 0, dvfMaxIDX[2]-1 )
            idx3[2] = np.clip( idx3[2], 0, dvfMaxIDX[2]-1 )
            idx4[2] = np.clip( idx4[2], 0, dvfMaxIDX[2]-1 )
            idx5[2] = np.clip( idx5[2], 0, dvfMaxIDX[2]-1 )
            idx6[2] = np.clip( idx6[2], 0, dvfMaxIDX[2]-1 )
            idx7[2] = np.clip( idx7[2], 0, dvfMaxIDX[2]-1 )
            idx8[2] = np.clip( idx8[2], 0, dvfMaxIDX[2]-1 )
             
            Dx = continuousIdxs[0] - idx1[0]
            Dy = continuousIdxs[1] - idx1[1]
            Dz = continuousIdxs[2] - idx1[2]
             
            w1 = (1.0 - Dx) * (1.0 - Dy) * (1.0 - Dz)
            w2 = (1.0 - Dx) * Dy         * (1.0 - Dz)
            w3 = Dx         * Dy         * (1.0 - Dz)
            w4 = Dx         * (1.0 - Dy) * (1.0 - Dz)
            w5 = (1.0 - Dx) * (1.0 - Dy) * Dz
            w6 = (1.0 - Dx) * Dy         * Dz
            w7 = Dx         * Dy         * Dz
            w8 = Dx         * (1.0 - Dy) * Dz
             
            #
            # convert for fast indexing
            #
            idx1 = ( idx1[0], idx1[1], idx1[2] )
            idx2 = ( idx2[0], idx2[1], idx2[2] )
            idx3 = ( idx3[0], idx3[1], idx3[2] )
            idx4 = ( idx4[0], idx4[1], idx4[2] ) 
            idx5 = ( idx5[0], idx5[1], idx5[2] ) 
            idx6 = ( idx6[0], idx6[1], idx6[2] )
            idx7 = ( idx7[0], idx7[1], idx7[2] )
            idx8 = ( idx8[0], idx8[1], idx8[2] )
    
            uxFullRes = (   w1 * ux[ idx1 ] 
                          + w2 * ux[ idx2 ]  
                          + w3 * ux[ idx3 ] 
                          + w4 * ux[ idx4 ] 
                          + w5 * ux[ idx5 ] 
                          + w6 * ux[ idx6 ] 
                          + w7 * ux[ idx7 ] 
                          + w8 * ux[ idx8 ] ) 
            
            uyFullRes = (   w1 * uy[ idx1 ] 
                          + w2 * uy[ idx2 ]  
                          + w3 * uy[ idx3 ] 
                          + w4 * uy[ idx4 ] 
                          + w5 * uy[ idx5 ] 
                          + w6 * uy[ idx6 ] 
                          + w7 * uy[ idx7 ] 
                          + w8 * uy[ idx8 ] ) 
            
            uzFullRes = (   w1 * uz[ idx1 ] 
                          + w2 * uz[ idx2 ]  
                          + w3 * uz[ idx3 ] 
                          + w4 * uz[ idx4 ] 
                          + w5 * uz[ idx5 ] 
                          + w6 * uz[ idx6 ] 
                          + w7 * uz[ idx7 ] 
                          + w8 * uz[ idx8 ] ) 
            
            
            dxFR = np.abs(niiAffineMatrixImage[0,0])# / 1000.0
            dyFR = np.abs(niiAffineMatrixImage[1,1])# / 1000.0
            dzFR = np.abs(niiAffineMatrixImage[2,2])# / 1000.0
            
            idxFullResX = idxFullResX.reshape( niiImageData.shape )
            idxFullResY = idxFullResY.reshape( niiImageData.shape )
            idxFullResZ = idxFullResZ.reshape( niiImageData.shape )
            
            # Axis aligned DVF?
            uxFullRes = uxFullRes.reshape( niiImageData.shape ) / dxFR + idxFullResX
            uyFullRes = uyFullRes.reshape( niiImageData.shape ) / dyFR + idxFullResY
            uzFullRes = uzFullRes.reshape( niiImageData.shape ) / dzFR + idxFullResZ
            
            warpedImgOut = ndimage.map_coordinates( niiImageData, [uxFullRes, uyFullRes, uzFullRes], order=1, mode='nearest' )
               
        if outNiiImageFileName != None:
            print( 'Saving warped image to %s' % outNiiImageFileName )
            saveNiftiImage(outNiiImageFileName, warpedImgOut, niiAffineMatrixImage )
        
        return warpedImgOut
 


        
        